package facci.leninanchundia.pizzaamor2.activities.client.payments.form

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import facci.leninanchundia.pizzaamor2.R

class ClientPaymentFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client_payment_form)
    }
}