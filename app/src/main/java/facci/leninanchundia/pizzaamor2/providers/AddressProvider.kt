package facci.leninanchundia.pizzaamor2.providers

import android.util.Log
import facci.leninanchundia.pizzaamor2.api.ApiRoutes
import facci.leninanchundia.pizzaamor2.models.Address
import facci.leninanchundia.pizzaamor2.models.Category
import facci.leninanchundia.pizzaamor2.models.ResponseHttp
import facci.leninanchundia.pizzaamor2.routes.AddressRoutes
import facci.leninanchundia.pizzaamor2.routes.CategoriesRoutes
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import java.io.File

class AddressProvider(val token: String) {

    private var addressRoutes: AddressRoutes? = null

    init {
        val api = ApiRoutes()
        addressRoutes = api.getAddressRoutes(token)
    }

    fun getAddress(idUser: String): Call<ArrayList<Address>>? {
        return addressRoutes?.getAddress(idUser, token)
    }

    fun create(address: Address): Call<ResponseHttp>? {
        return addressRoutes?.create(address, token)
    }
}